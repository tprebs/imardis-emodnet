package org.imardis.hackathon.diveapp.wreck;

import java.util.HashMap;
import java.util.Map;

public class WreckConverter {

    public Map<String, String> convert(String rawObj){
        Map<String,String> map = new HashMap<>();

        String[] lines = rawObj.split("~");
        String[] header = lines[0].split("\\|");
        String[] dataLine = lines[1].split("\\|");
        for(int i= 0; i <dataLine.length;i++){
            map.put(header[i], dataLine[i]);
        }
        return map;

    }
}
