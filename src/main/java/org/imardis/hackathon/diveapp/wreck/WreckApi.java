package org.imardis.hackathon.diveapp.wreck;

import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

@Component
public class WreckApi {

    public static class WreckResponse{

        Map<String,String> body;
        boolean exists;

        public WreckResponse(Map<String,String> body, boolean exists) {
            this.body = body;
            this.exists = exists;
        }

        public Map<String,String> getBody() {
            return body;
        }

        public boolean isExists() {
            return exists;
        }
    }

    public WreckResponse getWreck(String longitude, String latitude){
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        MultiValueMap<String, String> map= new LinkedMultiValueMap<>();
        map.add("theX", longitude);
        map.add("theY", latitude);
        map.add("nofPoints", "1");
        map.add("layer", "shipwrecks");
        map.add("atMaxZoom", "no");

        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);

        ResponseEntity<String> response = restTemplate.postForEntity( "http://www.emodnet-humanactivities.eu/mapping/getMapDataAttsMulti2.php", request , String.class );

        WreckConverter wreckConverter = new WreckConverter();

        boolean exists = response.getStatusCode().is2xxSuccessful() && response.getBody().contains("Access_res");
        Map<String,String> body = new HashMap<>();
        if(exists){
            body = wreckConverter.convert(response.getBody());
        }
        return new WreckResponse(body, exists);
    }

}
