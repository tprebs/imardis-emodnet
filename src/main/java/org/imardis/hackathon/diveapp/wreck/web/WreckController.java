package org.imardis.hackathon.diveapp.wreck.web;

import org.imardis.hackathon.diveapp.water.WaterQualityApi;
import org.imardis.hackathon.diveapp.water.WaterQualityData;
import org.imardis.hackathon.diveapp.wreck.WreckApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
public class WreckController {

    @Autowired
    private WreckApi wreckApi;

    @Autowired
    private WaterQualityApi waterQualityApi;

    @RequestMapping(method = RequestMethod.GET, path = "/wreck", produces = "application/json")
    public @ResponseBody
            ResponseEntity<Map<String, Object>> getWreck(@RequestParam(name = "long") String longitude, @RequestParam(name = "lat") String latitude){
        WreckApi.WreckResponse response = wreckApi.getWreck(longitude, latitude);
        if(response.isExists()){
            Map<String,Object> body = new HashMap<>();
            body.put("wreck", response.getBody());
            WaterQualityData waterQualityData = waterQualityApi.getWaterQuality(longitude, latitude).getBody();
            body.put("waterQuality", waterQualityData);
            return ResponseEntity.ok(body);
        }
        return ResponseEntity.notFound().build();
    }
}
