package org.imardis.hackathon.diveapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DiveappApplication {

	public static void main(String[] args) {
		SpringApplication.run(DiveappApplication.class, args);
	}
}
