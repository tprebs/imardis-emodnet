package org.imardis.hackathon.diveapp.water;

import com.vividsolutions.jts.geom.Coordinate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class WaterQualityApi {

    public static List<WaterQualityData> dataList = new ArrayList<>();


    public static class WaterQualityResponse{

        WaterQualityData body;

        public WaterQualityResponse(WaterQualityData body) {
            this.body = body;
        }

        public WaterQualityData getBody() {
            return body;
        }
    }

    public WaterQualityResponse getWaterQuality(String longitude, String latitude){
        if(dataList.isEmpty()){
            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

            MultiValueMap<String, String> map= new LinkedMultiValueMap<>();
            map.add("layers", "layers:bathingwaters_excellent|bathingwaters_good|bathingwaters_poor|bathingwaters_sufficient|bathingwaters_other");

            HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);

            ResponseEntity<String> response = restTemplate.postForEntity( "http://www.emodnet-humanactivities.eu/mapping/getMapDataCluster2.php", request , String.class );
            WaterQualityConverter converter = new WaterQualityConverter();
            dataList = converter.convert(response.getBody());
        }

        Coordinate coordToMatch = new Coordinate(Double.parseDouble(longitude), Double.parseDouble(latitude));

        Optional<WaterQualityData> matched = dataList.stream().sorted((o1, o2) -> {
            if(o1.getCoordinate().distance(coordToMatch) < o2.getCoordinate().distance(coordToMatch)){
                return -1;
            }
            return 1;
        }).findFirst();


        return new WaterQualityResponse(matched.get());
    }

}
