package org.imardis.hackathon.diveapp.water;


import com.vividsolutions.jts.geom.Coordinate;

import java.util.ArrayList;
import java.util.List;

public class WaterQualityConverter {

    public List<WaterQualityData> convert(String rawObj){
        List<WaterQualityData> coordinates = new ArrayList<>();

        String[] lines = rawObj.split("~");
        for(String line : lines){
            String[] dataLine = line.split("\\|");
            String quality = dataLine[0] ;
            for(int i= 1; i <dataLine.length;i++){
                String rawCoord = dataLine[i];
                String[] splitCoord = rawCoord.split(" ");
                double latitude = Double.parseDouble(splitCoord[0].split("_")[0]);
                double longitude = Double.parseDouble(splitCoord[1].split("_")[0]);

                coordinates.add(new WaterQualityData(new Coordinate(latitude,longitude),quality));
            }
        }

        return coordinates;

    }
}
