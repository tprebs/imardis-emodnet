package org.imardis.hackathon.diveapp.water;

import com.vividsolutions.jts.geom.Coordinate;

public class WaterQualityData {

    Coordinate coordinate;
    String quality;

    public WaterQualityData(Coordinate coordinate, String quality) {
        this.coordinate = coordinate;
        this.quality = quality;
    }

    public Coordinate getCoordinate() {
        return coordinate;
    }

    public String getQuality() {
        return quality;
    }
}
